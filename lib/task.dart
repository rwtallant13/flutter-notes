import 'taskDatabase.dart';
import 'main.dart';

class Task extends Element {
  String _title;
  String _text;

  Task({int id = -1, String title = "", String text = ""}) : super(id: id) {
    this._title = title;
    this._text = text;
  }

  get title => _title;
  get text => _text;

  set text(String s) {
    //assert(s.length > 0);
    _text = s;
  }

  set title(String s) {
    //assert(s.length > 0);
    _title = s;
  }

  Map<String, dynamic> toMapNoID() {
    return {
      TaskDatabase.columnTitle: _title,
      TaskDatabase.columnDue: _text
    };
  }

  Map<String, dynamic> toMap() {
    return {
      TaskDatabase.columnId: id,
      TaskDatabase.columnTitle: _title,
      TaskDatabase.columnDue: _text
    };
  }

  @override
  String toString() {
    return '\nTask{\nid: $id, \ntitle: $_title, \ntext: $_text\n}';
  }

  Task.fromMap(Map<String, dynamic> map) {
    id = map[TaskDatabase.columnId];
    _title = map[TaskDatabase.columnTitle];
    _text = map[TaskDatabase.columnDue];
  }
}