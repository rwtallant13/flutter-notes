import 'dart:async';
import 'package:flutter/material.dart';
import 'noteDatabase.dart';
//import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'note.dart';

// Define a Custom Form Widget
class NewNotePage extends StatefulWidget {
  Note _note;

  NewNotePage(this._note);

  @override
  _NewNotePageState createState() => _NewNotePageState(_note);
}

// Define a corresponding State class. This class will hold the data related to
// our Form.
class _NewNotePageState extends State<NewNotePage> {
  // Create a text controller. We will use it to retrieve the current value
  // of the TextField!
  TextEditingController noteController;
  TextEditingController titleController;

  Note _note;

  _NewNotePageState(this._note) {
    noteController = TextEditingController(text: _note.text);
    titleController = TextEditingController(text: _note.title);
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    noteController.dispose();
    super.dispose();
    NoteDatabase db = NoteDatabase.get();
    _note.title = titleController.text;
    _note.text = noteController.text;
    if(titleController.text.length>0 || noteController.text.length>0) {
      _note.id < 0 ? db.insert(_note) : db.update(_note);
    }
    //print("\n\nNotes and stuff doing things");
  }

  @override
  Widget build(BuildContext context) {
    //Note note = Note();

    return Scaffold(
      appBar: AppBar(
        title: Text('New Note'),
      ),
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
//            IconButton(icon: Icon(Icons.menu), onPressed: () {},),
//            IconButton(
//              //icon: Icon(Icons.save),
//              onPressed: () {
//                NoteDatabase db = NoteDatabase.get();
//                if(titleController.text.length != 0 && noteController.text.length != 0) {
//                  db.insertNote(new Note(
//                      id: -1,
//                      title: titleController.text,
//                      text: noteController.text)
//                  );
//                }
//              },
//            ),
          ],
        ),
      ),
      body: new Container(
        padding: new EdgeInsets.all(10.0),
        //decoration: new BoxDecoration(color: snapshot.data ?), //
        child: new Center(
          child: new Column(
            children: <Widget>[
              new TextFormField(
                style: Theme.of(context).textTheme.headline,
                maxLines: 2,
                controller: titleController,
                keyboardType: TextInputType.text,
                decoration: new InputDecoration.collapsed(
                  hintText: ('Title...'),
                ),
              ),
              new Expanded(
                child: new TextFormField(
                  expands: true,
                  maxLines: null,
                  minLines: null,
                  controller: noteController,
                  keyboardType: TextInputType.multiline,
                  decoration: new InputDecoration.collapsed(
                    hintText: ('Note...'),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}