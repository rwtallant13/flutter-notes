import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'newTaskPage.dart';
import 'taskDatabase.dart';
import 'task.dart';
import 'main.dart';



/// This is the stateless widget for the settings page
class TasksPage extends StatefulWidget {

  HomePageState home;
  TasksPage(this.home);

  @override
  _TasksPageState createState() => _TasksPageState(home);
}

class _TasksPageState extends State<TasksPage> {

  HomePageState home;
  _TasksPageState(this.home);

  @override
  Widget build(BuildContext context) {
    TaskDatabase db = TaskDatabase.get();
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => NewTaskPage(new Task())),
          );
        },
        tooltip: 'New Note',
        child: Icon(Icons.add),
        elevation: 2.0,
      ),
      body: new FutureBuilder(
        future: db.getItems(),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if(!snapshot.hasData) {
            return new Card();
          }

          if (snapshot.data.length > 0) {
            List<Task> content = snapshot.data;
            print(snapshot.data.length);

            return new Container(
              child: ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  if ((home.indexes).contains(content[index].id)) {
                    content[index].isSelected = true;
                  }
                  return new InkWell(
                    onTap: () {
                      if (home.longPress) {
                        home.setLongPress();
                        home.onElementSelected(content[index]);
                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  NewTaskPage(content[index])),
                        );
                      }
                    },
                    onLongPress: () {
                      home.setLongPress();
                      home.onElementSelected(content[index]);
                    },
                    child: new Card(
                        color: content[index].isSelected
                            ? Colors.grey
                            : Colors.blue,
                      elevation: 0,
                      child: Container(
                        alignment: Alignment(-1.0, 0.0),
                        height: 100,
                        width: double.infinity,
                        child: new Padding(
                          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
                          child:new Column(
                            children: <Widget>[
                              new RichText(
                                textAlign: TextAlign.left,
                                text: TextSpan(
                                  text: '${content[index].title}',
                                  style: DefaultTextStyle
                                    .of(context)
                                    .style
                                    .apply(
                                      fontSizeFactor: 2.0,
                                      fontWeightDelta: 2,
                                    ),
                                ),
                                overflow: TextOverflow.fade,
                                //maxLines: _lineSizer(content[index].text.length),
                                softWrap: true,

                              ),
                              new Padding(
                                  padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
                                  child: new RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                      text:'${content[index].text}',
                                      style: DefaultTextStyle
                                          .of(context)
                                          .style
                                          .apply(
                                        fontSizeFactor: 1,
                                        fontWeightDelta: 0,
                                      ),
                                    ),
                                    strutStyle: StrutStyle(
                                      forceStrutHeight: true,
                                    ),
                                    overflow: TextOverflow.fade,
                                    //maxLines: _lineSizer(content[index].text.length),
                                    softWrap: true,
                                  )
                              )
                            ]
                          )
                        )
                      )
                    ),
                  );
                },
              )
            );
          }
          else{
            return new Container();
          }
        }
      )
    );
  }
}