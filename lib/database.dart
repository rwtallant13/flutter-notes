import "main.dart";

abstract class DB {

  Future<void> remove(int id);

  Future<Element> getItem(int id);
  Future<List<Element>> getItems();

}