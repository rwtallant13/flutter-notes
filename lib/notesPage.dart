import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'note.dart';
import 'main.dart';
import 'newNotePage.dart';
import 'noteDatabase.dart';
import 'dart:math';

class NotesPage extends StatefulWidget {
  HomePageState home;
  NotesPage(this.home);

  @override
  _NotesPageState createState() => _NotesPageState(home);
}

class _NotesPageState extends State<NotesPage> {
  HomePageState home;
  _NotesPageState(this.home);

  double _cardSizer(int length) {
    // @TODO make screen dependent
    if (length < 1) {
      return 1;
    }
    if (length < 100) {
      return 0.75;
    }
    if (length < 200) {
      return 1;
    }
    if (length < 300) {
      return 2;
    }
    if (length < 400) {
      return 3;
    }
    return 3;
  }

  int _lineSizer(int length) {
    // @TODO make screen dependent
    if (length < 1) {
      return 2;
    }
    if (length < 200) {
      return 3;
    }
    if (length < 300) {
      return 10;
    }
    if (length < 400) {
      return 17;
    }
    return 16;
  }

  @override
  Widget build(BuildContext context) {
    NoteDatabase db = NoteDatabase.get();
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => NewNotePage(new Note())),
            );
          },
          tooltip: 'New Note',
          child: Icon(Icons.add),
          elevation: 2.0,
        ),
        body: new FutureBuilder(
            future: db.getItems(),
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (!snapshot.hasData) {
                return new Card();
              }
              if (snapshot.data.length > 0) {
                List<Note> content = snapshot.data;

                return new StaggeredGridView.countBuilder(
                  crossAxisCount: 4,
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    // reset selected state to last known state stored in home
                    if ((home.indexes).contains(content[index].id)) {
                      content[index].isSelected = true;
                    }
                    return new Card(
                        color: content[index].isSelected
                            ? Colors.grey
                            : Colors.blue,
                        child: InkWell(
                            onTap: () {
                              if (home.longPress) {
                                home.setLongPress();
                                home.onElementSelected(content[index]);
                              } else {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          NewNotePage(content[index])),
                                );
                              }
                            },
                            onLongPress: () {
                              home.setLongPress();
                              home.onElementSelected(content[index]);
                            },
                            child: new Container(
                                alignment: Alignment(-1.0, 0.0),
                                child: new Column(
                                  children: <Widget>[
                                    new Flexible(
                                      flex: 40,
                                      fit: FlexFit.loose,
                                      child: new Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 3.0, horizontal: 10.0),
                                        child: new RichText(
                                          text: TextSpan(
                                            text: '${content[index].title}',
                                            style: DefaultTextStyle.of(context)
                                                .style
                                                .apply(
                                                  fontSizeFactor: 1.5,
                                                  fontWeightDelta: 2,
                                                ),
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                          //minLines: 2,
                                          maxLines: 1,
                                          // @TODO line sizer does not apply to title
                                          softWrap: true,
                                        ),
                                      ),
                                    ),
                                    new Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 2.0, horizontal: 10.0),
                                        child: new RichText(
                                          text: TextSpan(
                                            text: '${content[index].text}',
                                            style: DefaultTextStyle.of(context)
                                                .style
                                                .apply(
                                                  fontSizeFactor: 1,
                                                  fontWeightDelta: 0,
                                                ),
                                          ),
                                          overflow: TextOverflow.fade,
                                          maxLines: _lineSizer(
                                              content[index].text.length),
                                          softWrap: true,
                                        ))
                                  ],
                                ))));
                  },
                  staggeredTileBuilder: (int index) => new StaggeredTile.count(
                      2,
                      _cardSizer(content[index].text.length +
                          content[index].title.length)),
                  mainAxisSpacing: 2.0,
                  crossAxisSpacing: 4.0,
                );
              } else {
                return new Card();
              }
            }));
  }
}
