import 'noteDatabase.dart';
import 'main.dart';

class Note extends Element{
  String _title;
  String _text;

  Note({int id = -1, String title = "", String text = ""}) : super(id: id) {
    this._title = title;
    this._text = text;
  }

  get title => _title;
  get text => _text;

  set text(String s) {
    //assert(s.length > 0);
    _text = s;
  }

  set title(String s) {
    //assert(s.length > 0);
    _title = s;
  }

  Map<String, dynamic> toMapNoID() {
    return {
      NoteDatabase.columnTitle: _title,
      NoteDatabase.columnText: _text
    };
  }

  Map<String, dynamic> toMap() {
    return {
      NoteDatabase.columnId: id,
      NoteDatabase.columnTitle: _title,
      NoteDatabase.columnText: _text
    };
  }

  @override
  String toString() {
    return '\nNote{\nid: $id, \ntitle: $_title, \ntext: $_text\n}';
  }

  Note.fromMap(Map<String, dynamic> map) {
    id = map[NoteDatabase.columnId];
    _title = map[NoteDatabase.columnTitle];
    _text = map[NoteDatabase.columnText];
  }
}