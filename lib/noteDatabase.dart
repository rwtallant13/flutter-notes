import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'note.dart';
import 'database.dart';

class NoteDatabase implements DB {
  static final NoteDatabase _noteDatabase = new NoteDatabase._internal();
  static final String _tableName = "notes";
  static final String _columnId = "id";
  static final String _columnTitle = "title";
  static final String _columnText = "text";

  Database database;
  bool didInit = false;

  static String get tableName => _tableName;
  static String get columnId => _columnId;
  static String get columnTitle => _columnTitle;
  static String get columnText => _columnText;

  NoteDatabase._internal();

  @override
  static NoteDatabase get() {
    return _noteDatabase;
}

  Future<Database> _getDb() async{
    if(!didInit) await _init();
    return database;
  }

  Future _init() async {
    database = await openDatabase(
      join(await getDatabasesPath(), 'Notes.db'),
      onCreate: (db, version) {
        return db.execute("CREATE TABLE $_tableName($_columnId INTEGER PRIMARY KEY, $_columnTitle TEXT, $_columnText TEXT)",);
      }, version: 1,
    );
    didInit = true;
  }

  Future<void> insert(Note n) async {
    final Database db = await _getDb();
    n.id = await  db.insert(
        _tableName,
        n.toMapNoID(),
        conflictAlgorithm: ConflictAlgorithm.replace
    );
  }

  Future<void> update(Note n) async {
    final Database db = await _getDb();
    await db.rawInsert(
      'INSERT OR REPLACE INTO '
          '$_tableName(${_columnId}, ${_columnTitle}, ${_columnText})'
          ' VALUES("${n.id}", "${n.title}", "${n.text}")'
    );
  }

  @override
  Future<void> remove(int id) async {
    final Database db = await _getDb();
    await db.delete(
      _tableName,
      where: '$_columnId = ?',
      whereArgs: [id]
    );
  }

  @override
  Future<Note> getItem(int id) async {
    final Database db = await _getDb();
    List<Map> maps = await db.query(
      _tableName,
      columns: [_columnId, _columnTitle, _columnText],
      where: '$_columnId = ?',
      whereArgs: [id]
    );
    if (maps.length > 0){
      return Note.fromMap(maps.first);
    }
    return null;
  }

  @override
  Future<List<Note>> getItems() async {

    final Database db = await _getDb();

    final List<Map<String, dynamic>> maps = await db.query(_tableName);

    return List.generate(maps.length, (i) {
      return Note(
        id: maps[i][_columnId],
        title: maps[i][_columnTitle],
        text: maps[i][_columnText]
      );
    });
  }
}