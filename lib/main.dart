import 'dart:async';
import 'package:flutter/material.dart';
import 'note.dart';
import "notesPage.dart";
import 'tasksPage.dart';
import 'noteDatabase.dart';
import 'taskDatabase.dart';
import 'database.dart';

void main() => runApp(NotesApp());

///Using Bloc
class NotesApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new HomePageState();
}

class HomePageState extends State<NotesApp> with TickerProviderStateMixin {
  Bloc _bloc = Bloc();
  bool _longPress = false;
  int _selected = 0;
  List<int> _indexes = new List();

  final List<MyTabs> _tabs = [new MyTabs(title: "Notes",),
    new MyTabs(title: "Tasks",)
  ];
  MyTabs _myHandler ;
  TabController _controller ;

  get indexes => _indexes;
  get longPress => _longPress;

  void setLongPress() {
    setState(() {
      _longPress = true;
    });
  }

  void onElementSelected(Element e) {
    setState(() {
      if (_indexes.contains(e.id)) {
        _selected--;
        _indexes.remove(e.id);
        if (_selected <= 0) _longPress = false;
      } else {
        _selected++;
        _indexes.add(e.id);
      }
      e.isSelected = !e.isSelected;
    });
  }


  void initState() {
    super.initState();
    _controller = new TabController(length: 2, vsync: this);
    _myHandler = _tabs[0];
    _controller.addListener(_handleSelected);
  }

  void _handleSelected() {
    setState(() {
      _myHandler= _tabs[_controller.index];
      _indexes.clear();
      _selected = 0;
      _longPress = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _bloc.darkThemeEnabled,
        initialData: _bloc.darkTheme,
        builder: (context, snapshot) => MaterialApp(
            theme: snapshot.data ? ThemeData.dark() : ThemeData.light(),
            home: Scaffold(
              drawer: Drawer(
                child: ListView(
                  children: <Widget>[
                    new UserAccountsDrawerHeader(
                      accountName: new Text("account,name"),
                      accountEmail: new Text("account@email"),
                      onDetailsPressed: () {},
                    ),
                    ListTile(
                      title: Text("Dark Theme"),
                      trailing: Switch(
                        value: snapshot.data,
                        onChanged: _bloc.changeTheme,
                      ),
                    )
                  ],
                ),
              ),
              appBar: _longPress
                  ? AppBar(
                //centerTitle: true,
                  backgroundColor: Colors.grey,
                  title: Text(_myHandler.title+" ($_selected)"),
                  bottom: PreferredSize(
                    preferredSize: Size.fromHeight(kTextTabBarHeight),
                    child: AppBar(
                      backgroundColor: Colors.grey,
                      leading: (IconButton(
                        icon: Icon(Icons.close),
                        color: Colors.white,
                        //iconSize: 35,
                        onPressed: () {
                          setState(() {
                            _selected = 0;
                            _indexes.clear();
                            _longPress = false;
                          });
                        },
                      )
                      ),
                      actions: <Widget>[
                        IconButton(
                          icon: Icon(Icons.delete),
                          color: Colors.white,
                          //iconSize: 30,
                          onPressed: () {
                            DB db;
                            switch(_controller.index) {
                              case 0: db = NoteDatabase.get(); break;
                              case 1: db = TaskDatabase.get(); break;
                            }
                            for (int i in _indexes) {
                              db.remove(i);
                            }
                            setState(() {
                              _selected = 0;
                              _indexes.clear();
                              _longPress = false;
                            });
                          },
                        )
                      ],
                    ),
                  )
              )
                  : AppBar(
                  title: Text(_myHandler.title),
                  //centerTitle: true,
                  bottom: PreferredSize(
                      preferredSize: Size.fromHeight(kTextTabBarHeight),
                      child: new TabBar(
                          controller: _controller,

                          tabs: <Tab>[
                            new Tab(
                              icon: new Icon(Icons.note),
                              //text: _tabs[0].title,
                            ),
                            new Tab(
                              icon: new Icon(Icons.done_all),
                              //text: _tabs[1].title,
                            ),
                          ]
                      )
                  )
              ),

              body: TabBarView(
                controller: _controller,
                children: <Widget>[NotesPage(this), TasksPage(this)],
              ),
            )));
  }
}

///Bloc class controls the runtime theme switch
class Bloc {
  final _themeController = StreamController<bool>();
  bool darkTheme = false;
  get changeTheme => _themeController.sink.add;
  get darkThemeEnabled => _themeController.stream;
}

class Element {
  int _id;
  bool isSelected = false;
  Element({int id = -1}) : _id = id;

  get id => _id;

  set id(int n) {
    assert(n >= 0);
    _id = n;
  }
}

class MyTabs {
  final String title;
  MyTabs({this.title});
}
