import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'note.dart';
import 'task.dart';
import 'database.dart';

class TaskDatabase implements DB {
  static final TaskDatabase _taskDatabase = new TaskDatabase._internal();
  static final String _tableName = "task";
  static final String _columnId = "id";
  static final String _columnTitle = "title";
  static final String _columnText = "due";

  Database database;
  bool didInit = false;

  static String get tableName => _tableName;
  static String get columnId => _columnId;
  static String get columnTitle => _columnTitle;
  static String get columnDue => _columnText;

  TaskDatabase._internal();

  static TaskDatabase get() {
    return _taskDatabase;
  }

  Future<Database> _getDb() async{
    if(!didInit) await _init();
    return database;
  }

  Future _init() async {
    database = await openDatabase(
      join(await getDatabasesPath(), 'Tasks.db'),
      onCreate: (db, version) {
        return db.execute("CREATE TABLE $_tableName($_columnId INTEGER PRIMARY KEY, $_columnTitle TEXT, $_columnText TEXT)",);
      }, version: 1,
    );
    didInit = true;
  }

  Future<void> insert(Task t) async {
    final Database db = await _getDb();
    t.id = await  db.insert(
        _tableName,
        t.toMapNoID(),
        conflictAlgorithm: ConflictAlgorithm.replace
    );
  }

  Future<void> update(Task n) async {
    final Database db = await _getDb();
    await db.rawInsert(
        'INSERT OR REPLACE INTO '
            '$_tableName(${_columnId}, ${_columnTitle}, ${_columnText})'
            ' VALUES("${n.id}", "${n.title}", "${n.text}")'
    );
  }

  @override
  Future<void> remove(int id) async {
    final Database db = await _getDb();
    await db.delete(
        _tableName,
        where: '$_columnId = ?',
        whereArgs: [id]
    );
  }

  Future<Task> getItem(int id) async {
    final Database db = await _getDb();
    List<Map> maps = await db.query(
        _tableName,
        columns: [_columnId, _columnTitle, _columnText],
        where: '$_columnId = ?',
        whereArgs: [id]
    );
    if (maps.length > 0){
      return Task.fromMap(maps.first);
    }
    return null;
  }

  Future<List<Task>> getItems() async {

    final Database db = await _getDb();

    final List<Map<String, dynamic>> maps = await db.query(_tableName);

    return List.generate(maps.length, (i) {
      return Task(
          id: maps[i][_columnId],
          title: maps[i][_columnTitle],
          text: maps[i][_columnText]
      );
    });
  }
}