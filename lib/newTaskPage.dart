import 'dart:async';
import 'package:flutter/material.dart';
//import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'task.dart';
import 'taskDatabase.dart';

// Define a Custom Form Widget
class NewTaskPage extends StatefulWidget {
  Task _task;
  NewTaskPage(this._task);

  @override
  _NewTaskPageState createState() => _NewTaskPageState(_task);
}

// Define a corresponding State class. This class will hold the data related to
// our Form.
class _NewTaskPageState extends State<NewTaskPage> {
  // Create a text controller. We will use it to retrieve the current value
  // of the TextField!
  TextEditingController taskController;
  TextEditingController titleController;

  Task _task;

  _NewTaskPageState(this._task) {
    taskController = TextEditingController(text: _task.text);
    titleController = TextEditingController(text: _task.title);
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    taskController.dispose();
    super.dispose();
    TaskDatabase db = TaskDatabase.get();
    _task.title = titleController.text;
    _task.text = taskController.text;
    if(titleController.text.length>0 || taskController.text.length>0) {
      _task.id < 0 ? db.insert(_task) : db.update(_task);
    }
    //print(titleController.text);
  }

  @override
  Widget build(BuildContext context) {

    Task task = Task();

    DateTime _date = new DateTime.now();
    TimeOfDay _time = new TimeOfDay.now();

    Future<Null> _selectDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
          context: context,
          initialDate: _date,
          firstDate: new DateTime(2016),
          lastDate: new DateTime(2021),
      );

      if(picked != null && picked != _date) {
        setState(() {
          _date = picked;
        });
        print('Selected ${_date.toString()}');

      }
    }

    Future<Null> _selectTime(BuildContext context) async {
      final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: _time,
      );

      if(picked != null && picked != _date) {
        setState(() {
          _time = picked;
        });
        print('Selected ${_time.toString()}');

      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('New Task'),
      ),
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(icon: Icon(Icons.menu), onPressed: () {},),
            IconButton(icon: Icon(Icons.save), onPressed: () {},),
          ],
        ),
      ),
      body: new Container(
        padding: new EdgeInsets.all(10.0),
        //decoration: new BoxDecoration(color: snapshot.data ?), //
        child: new Center(
          child: new Column(
            children: <Widget>[
              new TextFormField(
                style: Theme.of(context).textTheme.headline,
                maxLines: 2,
                controller: titleController,
                keyboardType: TextInputType.multiline,
                decoration: new InputDecoration.collapsed(
                  hintText: ('Title...'),
                ),
              ),
              new TextFormField(
                maxLines: 10,
                controller: taskController,
                keyboardType: TextInputType.multiline,
                decoration: new InputDecoration.collapsed(
                  hintText: ('Details...'),
                ),
              ),

              new RaisedButton(
                child: Text("Select date"),
                onPressed: (){
                  _selectDate(context);
                },
              ),
              new RaisedButton(
                child: Text("Select time"),
                onPressed: (){
                  _selectTime(context);
                },
              ),
              new Text("Task due: ${_date.toString()} ${_time.toString()}")
            ],
          ),
        ),
      ),
    );
  }
}